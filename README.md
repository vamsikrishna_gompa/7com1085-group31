# 7COM1085 – Research Methods

## Group - 31
Members: John Wesley Tata. Ganesh Manikanta Reddy. Nishantkumar Kiritbhai Patel. Chandra Haasini Gompa. Vamsikrishna Gompa

### Tasks:

* Research question **(due Friday 26th March 2021)**
* Search string and review protocol **(due Friday 23rd April 2021)**
* Literature search **(due Friday 30th April 2021)**
* Data extraction **(due Friday 7th May 2021)**
* Final report **(due Friday 14th May 2021)**

### Trello board link: 
https://trello.com/b/eD4ny64c/7com1085-group31

* Topic: Text summarization
* RQ: Which text summarization technique is more suited for summarizing news articles?
* Context: English news articles
* Population: All kinds of news articles 
* Intervention: Text summarization
* Comparison: Different text summarization techniques
* Outcome: Quality of the summary

### Search Protocol

Search string:
~~~~~ {.searchstring }
("Text summari*ation") AND ("News articles" OR "News websites")
~~~~~
Number of papers: 44

Inclusion criteria:

1. Any study that had used text summarization methods in the context of news related articles.

2. Considered both spelling versions of the word summarization.

3. Included the studies done on news articles and news websites.

Exclusion criteria:

1. Studies on multi-document summarization as it is out of the scope of our research.

2. Any study based on non-english articles.

3. Peer-reviewed studies.